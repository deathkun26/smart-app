import {
    ActivityIndicator, StyleSheet, View, Text, SafeAreaView,
    StatusBar, FlatList, TouchableOpacity, ImageBackground, Image, TouchableHighlight
} from 'react-native';

import ToggleSwitch from 'toggle-switch-react-native';

import React, { useEffect, useState } from 'react';

import RTCVoice from '@react-native-community/voice';

import axios from 'axios';

const AIO_FEED_IDS = ["proj-led", "proj-light"];
const AIO_USERNAME = "thkgq123";
const AIO_KEY = "aio_cGdw29jsFz6jc0uKK9zOCJapdCl8";

const Voice = RTCVoice;

export function HomeScreen({ route, navigation }) {
    //const { useVoice, listDevice } = route.params;
    const [isLoading, setLoading] = useState(true);
    const [device, setDevice] = useState([]);
    const [result, setResult] = useState('None')

    // Start voice implementation

    const onSpeechStartHandler = (e) => {
        console.log("start handler==>>>", e);
    }
    const onSpeechEndHandler = (e) => {
        console.log("stop handler", e);
    }

    const onSpeechResultsHandler = (e) => {
        let text = e.value[0];
        setResult(text);
        console.log("speech result handler", e);
    }

    const startRecording = async () => {
        try {
            const result = await Voice.start('en-Us');
            console.log(result);
            console.log(Voice);
        } catch (error) {
            console.log("error raised", error);
        }
    }

    const stopRecording = async () => {
        try {
            await Voice.stop();
        } catch (error) {
            console.log("error raised", error);
        }
    }


    // End voice implementation

    const renderItem = ({ item }) => (
        <View style={styles.item}>
            <ToggleSwitch
                isOn={item.data == 'Off' ? false : true}
                label={`${item.name} \t ${item.id}`}
                labelStyle={{ width: '80%', color: 'white' }}
                disabled={true}
            />
        </View>
    );

    const buttonClickedHandler = () => {
        console.log('You have been clicked a button!');
        // do something
    };

    const handleDeviceUpdated = (id, newData) => {
        const newDevice = list.map((item) => {
            if (item.id === id) {
                const updatedItem = {
                    "name": item.name,
                    "id": item.id,
                    "data": newData
                }
                return updatedItem;
            }

            return item;
        });

        setDevice(newDevice);
    }

    const getDevice = async () => {
        try {
            // const response = await axios.get(`https://io.adafruit.com/api/v2/${AIO_USERNAME}/dashboards/${AIO_FEED_IDS[0]}`);
            // console.log(response.data);

            json = {
                device: [
                    {
                        "id": "1",
                        "name": "LED",
                        "data": "On",
                    },
                    {
                        "id": "2",
                        "name": "LED",
                        "data": "Off",
                    },
                    {
                        "id": "3",
                        "name": "LED",
                        "data": "On",
                    },
                ]
            }
            setDevice(json.device);
        }
        catch (error) {
            console.log(error);
        } finally {
            setLoading(false);
            console.log("Loading finished");
        }
    }

    useEffect(() => {
        console.log(Voice);

        Voice.onSpeechStart = onSpeechStartHandler;
        Voice.onSpeechEnd = onSpeechEndHandler;
        Voice.onSpeechResults = onSpeechResultsHandler;



        const interval = setInterval(() => getDevice(), 10000);
        // //getDevice();
        return () => {
            clearInterval(interval);
            Voice.destroy().then(Voice.removeAllListeners);
        }
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.body}>
                {isLoading ? <ActivityIndicator /> : (
                    <FlatList
                        data={device}
                        keyExtractor={item => item.id}
                        renderItem={renderItem}
                    />
                )}
            </View>
            <View style={styles.footer}>
                <View>
                    <Text>Result : ${result}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={startRecording}
                        style={styles.roundButton}>
                        <Image
                            style={{ width: 20, height: 40 }}
                            source={require('../assets/voice.png')}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={stopRecording}
                        style={styles.roundButtonRed}>
                        <Image
                            style={{ width: 20, height: 40 }}
                            source={require('../assets/voice.png')}
                        />
                    </TouchableOpacity>
                </View>


            </View>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1760FC',
    },
    body: {
        flex: 1,
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    item: {
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        backgroundColor: '#407DFF',
    },
    footer: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    roundButton: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 100,
        backgroundColor: '#1760FC',
    },
    roundButtonRed: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 100,
        backgroundColor: 'red',
    },
});


