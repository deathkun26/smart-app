import {
    StyleSheet, View, Text, PermissionsAndroid, ActivityIndicator
} from 'react-native';

import React, { useEffect, useState } from 'react';

import axios from 'axios';

const AIO_FEED_IDS = ["proj-led", "proj-light"];
const AIO_USERNAME = "thkgq123";
const AIO_KEY = "aio_cGdw29jsFz6jc0uKK9zOCJapdCl8";
record_permission = false;

const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const grants = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);

            console.log('write external stroage', grants);

            if (
                grants['android.permission.WRITE_EXTERNAL_STORAGE'] ===
                PermissionsAndroid.RESULTS.GRANTED &&
                grants['android.permission.READ_EXTERNAL_STORAGE'] ===
                PermissionsAndroid.RESULTS.GRANTED &&
                grants['android.permission.RECORD_AUDIO'] ===
                PermissionsAndroid.RESULTS.GRANTED
            ) {
                record_permission = true;
                console.log('Permissions granted');
            } else {
                record_permission = false;
                console.log('All required permissions not granted');
                return;
            }
        } catch (err) {
            record_permission = false;
            console.warn(err);
            return;
        }
    }
};

export function WelcomeScreen({ navigation }) {
    const [device, setDevice] = useState([])

    requestCameraPermission();

    const getDevice = async () => {
        try {
            const response = await axios.get(`https://io.adafruit.com/api/v2/${AIO_USERNAME}/dashboards/${AIO_FEED_IDS[0]}`);
            //console.log(response.data);
            json = {
                device: [
                    {
                        "id": "1",
                        "name": "LED",
                        "data": "On",
                    },
                    {
                        "id": "2",
                        "name": "LED",
                        "data": "Off",
                    },
                    {
                        "id": "3",
                        "name": "LED",
                        "data": "Off",
                    },
                ]
            }
            setDevice();
        }
        catch (error) {
            console.log(error);
        } finally {
            console.log("Loading finished");
            navigation.navigate('Home', {
                listDevice: device,
                canUseVoice: record_permission,
            });
        }
    }

    useEffect(() => {
        getDevice();
    }, []);

    return (
        <View style={styles.container}>
            <Text>Welcome to Smart App</Text>
            <Text>Loading Device...</Text>
            <ActivityIndicator size="large" color="#1760FC" />
        </View>
    );
}

styles = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});