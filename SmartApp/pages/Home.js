import {
    ActivityIndicator, StyleSheet, View, Text, SafeAreaView, Platform,
    StatusBar, FlatList, TouchableOpacity, ImageBackground, Image, TouchableHighlight
} from 'react-native';

import ToggleSwitch from 'toggle-switch-react-native';

import React, { useEffect, useState } from 'react';

import Voice from '@react-native-voice/voice';

import axios from 'axios';



const AIO_FEED_IDS = ["proj-led", "proj-light"];
const AIO_USERNAME = "thkgq123";
const AIO_KEY = "aio_cGdw29jsFz6jc0uKK9zOCJapdCl8";

const numDevices = 3;

let isRecord = false;

export function HomeScreen({ route, navigation }) {
    const { canUseVoice, listDevice } = route.params;
    const [isLoading, setLoading] = useState(true);
    const [device, setDevice] = useState([]);
    const [result, setResult] = useState('None');
    const [buttonColor, setButtonColor] = useState('#1760FC');

    // Start voice implementation

    const onSpeechStartHandler = (e) => {
        console.log("start handler==>>>", e);
    }
    const onSpeechEndHandler = (e) => {
        console.log("stop handler", e);
    }

    const onSpeechResultsHandler = (e) => {
        let text = e.value[0];
        setResult(text);
        console.log("speech result handler", e);
        stopRecording();
    }

    const startRecording = async () => {
        setButtonColor('red');
        console.log("Start recording");
        try {
            await Voice.start('en-US');
        } catch (error) {
            console.log("error raised", error);
        }
    }

    const stopRecording = async () => {
        console.log("Stop recording");
        try {
            await Voice.stop();
        } catch (error) {
            console.log("error raised", error);
        }
        setButtonColor('#407DFF');
    }

    const buttonHandle = async () => {
        // if (!isRecord) {
        //     isRecord = true;
        //     setButtonColor('red');
        //     startRecording();
        // }
        // else {
        //     isRecord = false;
        //     setButtonColor('#407DFF');
        //     stopRecording();
        // }
        startRecording();
    }

    // End voice implementation

    const updateItem = async (item) => {

        let valueSend = {
            datum: {
                value: item.data == 'OFF' ? 'ON' : 'OFF',
            }
        };

        let headerSend = {
            headers: {
                "X-AIO-Key": AIO_KEY,
                "Content-Type": "application/json",
            },
        };

        let key = item.key;
        console.log(key);

        await axios
            .post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/${key}/data`,
                valueSend,
                headerSend
            )
            .then((response) => {
                // setproduct(response.data)
                console.log(response.data);
            });
    };

    const renderItem = ({ item }) => (
        <View style={styles.item}>
            <ToggleSwitch
                isOn={item.data == 'OFF' ? false : true}
                label={`${item.name} \t ${item.id}`}
                labelStyle={{ width: '80%', color: 'white' }}
                onToggle={() => updateItem(item)}
            />
        </View>
    );

    const getDevice = async () => {
        try {
            let devices = [];
            for (let i = 1; i <= numDevices; i++) {
                let new_device = {};
                const response = await axios.get(
                    `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-${i}`
                );
                new_device.key = `iot-led-${i}`;
                new_device.id = response.data.id;
                new_device.name = response.data.name;
                new_device.data = response.data.last_value;
                //console.log(device);
                devices.push(new_device);
            }
            //console.log(devices);
            setDevice(devices);
        }
        catch (error) {
            console.log(error);
        }
    }


    useEffect(() => {
        console.log("Home page");

        Voice.onSpeechStart = onSpeechStartHandler;
        Voice.onSpeechEnd = onSpeechEndHandler;
        Voice.onSpeechResults = onSpeechResultsHandler;

        let interval = null;
        if (isLoading) {
            interval = setInterval(() => getDevice(), 500);
            setLoading(false);
        }

        return () => {
            clearInterval(interval);
            Voice.destroy().then(Voice.removeAllListeners);
        }
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.body}>
                {isLoading ? <ActivityIndicator /> : (
                    <FlatList
                        data={device.length == 0 ? listDevice : device}
                        keyExtractor={item => item.id}
                        renderItem={renderItem}
                    />
                )}
            </View>
            <View style={styles.footer}>
                <View>
                    <Text>Result : {result}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 50 }}>
                    <TouchableOpacity
                        onPress={buttonHandle}
                        style={[styles.roundButton, { backgroundColor: buttonColor }]}>
                        <Image source={require("../assets/voice.png")} style={{ height: 40, width: 20 }}></Image>
                    </TouchableOpacity>
                </View>


            </View>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1760FC',
    },
    body: {
        flex: 2,
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    item: {
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        backgroundColor: '#407DFF',
    },
    footer: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    roundButton: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 100,
    },
});


