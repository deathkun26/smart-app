import {
    StyleSheet, View, Text, PermissionsAndroid, ActivityIndicator,
    Platform, Button
} from 'react-native';

import React, { useEffect, useState } from 'react';

import { StackActions } from '@react-navigation/native';

import axios from 'axios';

import * as Network from 'expo-network';

import NotificationPopup from 'react-native-push-notification-popup';

const AIO_FEED_IDS = ["proj-led", "proj-light"];
const AIO_USERNAME = "thkgq123";

record_permission = false;

const numDevices = 3;

const requestVoicePermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const grants = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
            );

            console.log('write external stroage', grants);

            if (
                grants['android.permission.RECORD_AUDIO'] ===
                PermissionsAndroid.RESULTS.GRANTED
            ) {
                record_permission = true;
                console.log('Permissions granted');
            } else {
                record_permission = false;
                console.log('All required permissions not granted');
                return;
            }
        } catch (err) {
            record_permission = false;
            console.warn(err);
            return;
        }
    }
};

export function WelcomeScreen({ navigation }) {
    const [error, setError] = useState(false);
    requestVoicePermission();

    const checkNetworkConnection = async () => {
        const result = await Network.getNetworkStateAsync();
        if (result.isConnected) {
            getDevice();
        }
        else {
            console.log("Not connect to network");
        }
    }

    const getDevice = async () => {
        try {
            let devices = [];
            for (let i = 1; i <= numDevices; i++) {
                let device = {};
                const response = await axios.get(
                    `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-${i}`
                );
                device.key = `iot-led-${i}`;
                device.id = response.data.id;
                device.name = response.data.name;
                device.data = response.data.last_value;
                console.log(device);
                devices.push(device);
            }

            // console.log(devices);

            console.log("Loading Devices finished");
            navigation.dispatch(
                StackActions.replace('Home', {
                    canUseVoice: record_permission,
                    listDevice: devices,
                }));
        }
        catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        checkNetworkConnection();
    }, []);

    return (
        <View style={styles.container}>
            <NotificationPopup ref={ref => this.popup = ref} />
            <Text>Welcome to Smart App</Text>
            {error ?
                (<View>
                    <Text style={{ color: 'red' }}>Something wrong. Please restart the App</Text>
                </View>) :
                (<View>
                    <Text>Loading Device...</Text>
                    <ActivityIndicator size="large" color="#1760FC" />
                </View>
                )}

        </View>
    );
}

styles = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});