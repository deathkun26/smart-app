import { Text, StyleSheet } from 'react-native';

import React, { useEffect, useState } from 'react';

import { WelcomeScreen } from './pages/Welcome';
import { HomeScreen } from './pages/Home';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Welcome" screenOptions={{
        headerStyle: {
          backgroundColor: '#1760FC',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{ title: 'Welcome', }}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Smart App', }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
